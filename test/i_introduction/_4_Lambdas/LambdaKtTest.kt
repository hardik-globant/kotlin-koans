package i_introduction._4_Lambdas

import data.Person
import org.junit.Assert.*
import org.junit.Test

/**
 * Created by hardik.trivedi on 14/05/18.
 */
class LambdaKtTest {
    @Test
    fun containsEven() {
        assertArrayEquals(arrayOf(2, 4, 6).toIntArray(), lambda0(arrayOf(1, 2, 3, 4, 5, 6)).toIntArray())
    }

    @Test
    fun doNotContainsEven() {
        assertEquals(0, lambda0(arrayOf(1, 3, 5, 7, 9)).size)
    }

    @Test
    fun nameStartsWithHAndLengthIsGreaterThanFour() {
        assertEquals(arrayOf("Hartley", "Hayden").toList(), lambda1(arrayOf("Hart", "Hartley", "Hayden", "Victoria", "Tom")))
    }

    @Test
    fun nameStartsWithHAndLengthIsGreaterThanFourAndCasesAreIgnored() {
        assertEquals(arrayOf("hartley", "hayden").toList(), lambda1(arrayOf("Hart", "hartley", "hayden", "Victoria", "Tom")))
    }

    @Test
    fun namesAreNotFoundWhichStartsWithHAndLengthIsGreaterThanFour() {
        assertEquals(0, lambda1(arrayOf("Victoria", "Tom")).size)
    }

    @Test
    fun sortedInDescendingOrder() {
        assertEquals(listOf(40, 20, 10), lambda2(mutableListOf(20, 40, 10)))
        assertEquals(listOf(10, 0, -5), lambda2(mutableListOf(-5, 0, 10)))
    }

    @Test
    fun personsNameAreSortedAscending() {
        val listOfPersons = mutableListOf<Person>()
        listOfPersons.add(Person(name = "Bharat", age = 29, weight = 75))
        listOfPersons.add(Person(name = "Rupesh", age = 28, weight = 82))
        listOfPersons.add(Person(name = "Hardik", age = 30, weight = 90))

        val sortedPersons = mutableListOf<Person>()
        sortedPersons.add(Person(name = "Bharat", age = 29, weight = 75))
        sortedPersons.add(Person(name = "Hardik", age = 30, weight = 90))
        sortedPersons.add(Person(name = "Rupesh", age = 28, weight = 82))
        assertEquals(sortedPersons, lambda3(listOfPersons))
    }


    @Test
    fun personAreSortedWithNameWhereAgeIsGreaterThanThirty() {

        val listOfPersons = mutableListOf<Person>()
        listOfPersons.add(Person(name = "Jack", age = 29, weight = 75))
        listOfPersons.add(Person(name = "Maximus", age = 35, weight = 102))
        listOfPersons.add(Person(name = "Hector", age = 32, weight = 90))

        val sortedPersons = mutableListOf<Person>()
        sortedPersons.add(Person(name = "Hector", age = 32, weight = 90))
        sortedPersons.add(Person(name = "Maximus", age = 35, weight = 102))
        assertEquals(sortedPersons, lambda4(listOfPersons))
    }

    @Test
    fun doublesTheNumberInList() {
        assertEquals(listOf(2, 4, 6, 8, 10), lambda5(listOf(1, 2, 3, 4, 5)))
    }
}